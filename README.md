# Lexicon overlap score usage examples
This repository contains usage examples for the lexicon overlap score. 

# Installation and usage
Dependency can be installed via `pip install -r requirements.txt`. 
If version conflicts occur, use requirements_fixed_version.txt.

Then use with jupyter notebook or jupyterlab.

# Datasets
For further analysis the sentiment140 dataset can be downloaded and put into the
`sentiment_test_sets` folder. The notebook `Classification Similarity Correlation.ipynb`
contains all necessary information. The file is not required for the execution of any example. 