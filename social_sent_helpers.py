import pandas as pd
from os import listdir
from os.path import isfile, join
from tqdm.notebook import tqdm

def read_lexicon(name):
    return pd.read_csv("lexicons\\"+ name +".tsv", names=["word", "so", "sd"], header=None, sep="\t", engine="python")

def list_lexicons():
    return pd.Series([f[:-4] for f in listdir("lexicons") if isfile(join("lexicons", f))]).sort_values()

def read_all_lexicons():
    return [read_lexicon(name) for name in tqdm(list_lexicons(), desc="Reading lexicons from disk")]

games_list = ["gaming", "Smite", "skyrim", "PS4", "xboxone", "Eve"]
sports_list = ["sports", "hockey", "Fitness", "baseball", "MMA", "bicycling"]